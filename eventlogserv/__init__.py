__author__ = 'Athanaziz'

import sqlite3
from contextlib import closing
from flask import Flask, g
app = Flask(__name__)
import eventlogserv.views

# configuration
DATABASE = '/tmp/els.db'
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'admin'

app.config.from_object(__name__)
# app.config.from_envvar('EVENTLOG_SETTINGS', silent=True)


def connect_db():
    return sqlite3.connect(app.config['DATABASE'])


def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql') as f:
            db.cursor().executescript(f.read())
        db.commit()


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()