drop table if exists logs;
create table logs (
  id integer primary key autoincrement,
  message text not null,
  category text not null,
  person text not null,
  creation_time integer(4) not null default (strftime('%s','now'))
);