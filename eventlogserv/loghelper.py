__author__ = 'Athanaziz'
import string


def make_arg_list(message):
    """

    :param message from client:
    :return: parameter list for query
    """
    category = 'all'
    person = 'all'
    hashpos = string.find(message, '#')
    atpos = string.find(message, '@')
    if hashpos > -1:
        category = message[hashpos + 1:].split()[0]
    if atpos > -1:
        person = message[atpos + 1:].split()[0]
    arglist = [message, category.lower(), person.lower()]
    return arglist