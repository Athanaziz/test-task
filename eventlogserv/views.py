__author__ = 'Athanaziz'
from eventlogserv import app
from flask import request, g, abort, make_response
import json
import loghelper


@app.route('/api/v1.0/logs', methods=['POST'])
def add_logs():
    if not request.data:
        abort(400)
    g.db.execute('insert into logs (message, category, person) values (?, ?, ?)', loghelper.make_arg_list(request.data))
    g.db.commit()
    print loghelper.make_arg_list(request.data)
    return make_response('Record created')


@app.route('/api/v1.0/logs', methods=['GET'])
def get_ten_logs():
    cur = g.db.execute('select * from logs order by creation_time desc limit 10')
    logs = []
    for row in cur.fetchall():
        logs.append(dict(id=row[0], message=row[1], category=row[2], person=row[3], creation_time=row[4]))
    if not logs:
        return make_response('No records here')
    log_key = str(request.args.get('sort'))
    if log_key is not None:
        logs.sort(key=lambda l: l.get(log_key), reverse=True)
    return make_response(json.dumps(logs))


@app.errorhandler(404)
def not_found(error):
    return make_response(json.dumps({'error': 'Not found'}), 404)