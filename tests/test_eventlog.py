__author__ = 'Athanaziz'

import os
import eventlogserv
import unittest
import tempfile
import json
import time


class EventLogTestsEmptyDB(unittest.TestCase):

    def setUp(self):
        self.db_fd, eventlogserv.app.config['DATABASE'] = tempfile.mkstemp()
        eventlogserv.app.config['TESTING'] = True
        self.client = eventlogserv.app.test_client()
        with eventlogserv.app.app_context():
            eventlogserv.init_db()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(eventlogserv.app.config['DATABASE'])

    def test_get_ten_if_empty(self):
        res = self.client.get('/api/v1.0/logs')
        assert b'No records here' in res.data

    def test_add_msg(self):
        res = self.client.post('/api/v1.0/logs',
                               data='Great article about content marketing artic.le')
        assert b'Record created' in res.data

    def test_add_msg_with_tags(self):
        res = self.client.post('/api/v1.0/logs',
                               data='cats are felines #cats @bobcat')
        assert b'Record created' in res.data

    def test_add_msg_weird_order(self):
        res = self.client.post('/api/v1.0/logs',
                               data='elephants are the biggest mammals@elephant#mammal')
        assert b'Record created' in res.data

    def test_add_msg_weird_order2(self):
        res = self.client.post('/api/v1.0/logs',
                               data='@elephants are the biggest mammals')
        assert b'Record created' in res.data

    def test_add_msg_weird_order3(self):
        res = self.client.post('/api/v1.0/logs',
                               data='#mammal elephants are the biggest mammals')
        assert b'Record created' in res.data


class EventLogTestsFullDB(unittest.TestCase):
    content_list = ['Do you know about A/B tests?',
                    'Boring meeting #job @dev',
                    'cats are felines #cats @bobcat',
                    'Great article about content marketing artic.le',
                    'dogs are canines #dog @beagle',
                    'Read books!!!',
                    'Winter is coming #weather @Ned',
                    'something happened today #signs @Lucy',
                    'something happened again! #signs @Lucy',
                    'Lets play football',
                    'Ive never been in Bialystok #travel @Xena',
                    'The man who passes the sentence should swing the sword #sword @Ned']

    def setUp(self):
        self.db_fd, eventlogserv.app.config['DATABASE'] = tempfile.mkstemp()
        eventlogserv.app.config['TESTING'] = True
        self.client = eventlogserv.app.test_client()
        with eventlogserv.app.app_context():
            eventlogserv.init_db()
        for item in self.content_list:
            self.client.post('/api/v1.0/logs', data=item)
            # time.sleep(1)

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(eventlogserv.app.config['DATABASE'])

    def test_get_ten_by_time(self):
        res = self.client.get('/api/v1.0/logs?sort=creation_time')
        d = json.loads(res.data)
        self.assertEqual(True, d[0].get('creation_time') > d[9].get('creation_time'))
        self.assertEqual(10, len(d))

    def test_get_ten_by_category(self):
        res = self.client.get('/api/v1.0/logs?sort=category')
        d = json.loads(res.data)
        self.assertEqual(b'weather', d[0].get('category'))
        self.assertEqual(10, len(d))

    def test_get_ten_by_person(self):
        res = self.client.get('/api/v1.0/logs?sort=person')
        d = json.loads(res.data)
        self.assertEqual(b'xena', d[0].get('person'))
        self.assertEqual(10, len(d))

if __name__ == '__main__':
    unittest.main()